import { IBook } from './../shared/models/IBook';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-book-cover',
  templateUrl: './book-cover.component.html',
  styleUrls: ['./book-cover.component.css']
})
export class BookCoverComponent implements OnInit {
  @Input() book: IBook;
  constructor() { }

  ngOnInit() {
  }

}

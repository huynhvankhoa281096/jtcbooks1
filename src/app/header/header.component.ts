import { LocalStorageService } from 'angular-2-local-storage';
import { IBook } from './../shared/models/IBook';
import { BookService } from './../shared/services/book.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isSearch: Boolean = false;
  results: IBook[] = [];
  bookService2: BookService;
  constructor(private bookService: BookService,
    private localStorage: LocalStorageService,
    private router: Router) {
  }

  ngOnInit() {
  }
  toggleSearch() {
    this.isSearch = !this.isSearch;
  }

  searchBook(title: string) {
    // this.results = this.bookService.searchBook(title);
  }
  logout() {
    this.localStorage.remove('login');
    this.router.navigate(['/login']);
  }
}

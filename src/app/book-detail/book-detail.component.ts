import { IBook } from './../shared/models/IBook';
import { BookService } from './../shared/services/book.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit {

  book: IBook = {};
  constructor(private route: ActivatedRoute, private bookService: BookService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const id = parseInt(params['id'] as string, 0);
      this.bookService.findOneBookById(id as number).subscribe(book => {
        this.book = book;
      });
    });
  }

}

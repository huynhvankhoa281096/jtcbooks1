import { AppGuardService } from './shared/services/app-guard.service';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { BookService } from './shared/services/book.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BannerComponent } from './banner/banner.component';
import { BookshelfComponent } from './bookshelf/bookshelf.component';
import { BookCoverComponent } from './book-cover/book-cover.component';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BookDetailComponent } from './book-detail/book-detail.component';
import { ListBookComponent } from './bookshelf/list-book/list-book.component';
import { GridBookComponent } from './bookshelf/grid-book/grid-book.component';
import { LoginComponent } from './auth/login/login.component';
import { LayoutComponent } from './layout/layout.component';
import { LocalStorageModule } from 'angular-2-local-storage';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BannerComponent,
    BookshelfComponent,
    BookCoverComponent,
    BookDetailComponent,
    PageNotFoundComponent,
    ListBookComponent,
    GridBookComponent,
    LoginComponent,
    LayoutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    LocalStorageModule.withConfig({
      prefix: 'app',
      storageType: 'localStorage'
    }),
    RouterModule.forRoot([
      { path: 'login', component: LoginComponent },
      {
        canActivate: [AppGuardService],
        path: '',
        component: LayoutComponent,
       
        children: [
          {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
          {path: 'dashboard', component: BannerComponent},
          {
            path: 'book-shelf',
            component: BookshelfComponent,
            children: [
              { path: 'grid', component: GridBookComponent },
              { path: 'list', component: ListBookComponent }
            ]
          },
          { path: 'book/:id', component: BookDetailComponent },
        ]
      },
      { path: '**', component: PageNotFoundComponent }
    ]),
    // RouterModule.forRoot([
    //   { path: '', component: BannerComponent},
    //   { path: 'books', component: BookshelfComponent },
    //   { path: '**', component: PageNotFoundComponent}
    // ]),
    ToastModule.forRoot()
  ],
  providers: [BookService, AppGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }

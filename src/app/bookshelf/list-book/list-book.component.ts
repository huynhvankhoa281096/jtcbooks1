import { BookService } from './../../shared/services/book.service';
import { IBook } from './../../shared/models/IBook';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-list-book',
  template: `
  <div class="list" *ngFor="let book of books">
    <app-book-cover  [book]="book"></app-book-cover>
    <div class="detail">
      Lorem ipsum dolor sit amet consectetur, adipisicing elit.
      Sequi fugit nesciunt qui libero cum sit porro officia rerum excepturi illum.
    </div>
  </div>
  `
  ,
  styleUrls: ['./list-book.component.css']
})
export class ListBookComponent implements OnInit {
  @Input() books: IBook[] = [];
  constructor(private bookService: BookService) { }

  ngOnInit() {
    this.bookService.getBooks().subscribe((books) => {
      this.books = this.books.concat(books);
    });
  }

}

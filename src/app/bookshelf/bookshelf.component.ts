import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { BookService } from './../shared/services/book.service';
import { IBook } from './../shared/models/IBook';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bookshelf',
  templateUrl: './bookshelf.component.html',
  styleUrls: ['./bookshelf.component.css']
})
export class BookshelfComponent implements OnInit {
  grid = true;
  isSearch = true;
  books: IBook[] = [];
  constructor(private bookService: BookService) {
    
    this.bookService.getBooks().subscribe((books) => {
      this.books = this.books.concat(books);
    });
  }

  ngOnInit() {
    this.loadBooks();
  }

  toggleSearch() {
    this.isSearch = !this.isSearch;
  }

  search(title: string) {
  }

  loadBooks() {
  }

  changeLayout(flag: boolean) {
    this.grid = flag;
  }
}

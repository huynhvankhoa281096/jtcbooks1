import { BookService } from './../../shared/services/book.service';
import { IBook } from './../../shared/models/IBook';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-grid-book',
  template: `<app-book-cover *ngFor="let book of books" [book]="book"></app-book-cover>`,
  styleUrls: ['./grid-book.component.css']
})
export class GridBookComponent implements OnInit {
  @Input() books: IBook[] = [];
  constructor(private bookService: BookService) { }

  ngOnInit() {
    this.bookService.getBooks().subscribe((books) => {
      this.books = this.books.concat(books);
    });
  }

}

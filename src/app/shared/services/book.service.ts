import { IBook } from './../models/IBook';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Injectable()
export class BookService {
  books: IBook[] = [];
  constructor(private http: Http, public toastr: ToastsManager) {
    this.getBooks().subscribe(books => {
      this.books = books;
    });
  }

  getBooks(): Observable<IBook[]> {
    const uri = 'http://demo6327714.mockable.io/book';
    return this.http.get(uri).map(res => {
      return res.json() as IBook[];
    }).do(data => {
      console.log('Thanh cong');
      this.toastr.success('Thành công');
    }, error => {
      this.toastr.error('Rớt mạng cmnr');
    });
  }
  searchBook(title: string) {
    return this.getBooks().map(books => {
      return books.filter(book => {
        return book.title.toLowerCase().includes(title.toLowerCase());
      });
    });
  }

  findOneBookById(id: number): Observable<IBook> {
    return this.getBooks().map(books => {
      return books.find(book => {
        return book.id === id;
      });
    });
  }
  addBook(book: IBook) {
    // this.books.push(book);
  }
}

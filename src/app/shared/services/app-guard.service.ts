import { LocalStorageService } from 'angular-2-local-storage';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
@Injectable()
export class AppGuardService implements CanActivate {

  constructor(private localStorage: LocalStorageService, private router: Router) { }

  canActivate() {
    const login = this.localStorage.get('login');
    if (!login) {
      this.router.navigate(['/login']);
    }
    return login ? true : false;
  }
}

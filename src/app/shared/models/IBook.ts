export interface IBook {
    image?: string;
    title?: string;
    link?: string;
    id?: number;
}

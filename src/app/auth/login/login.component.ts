import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login: ILogin = {
    email: '',
    password: ''
  };
  constructor(private localStorage: LocalStorageService, private router: Router) { }

  ngOnInit() {
  }

  goLogin() {
    console.log(this.login);
    this.localStorage.add('login', this.login);
    this.router.navigate(['/book-shelf']);
  }

}

interface ILogin {
  email: string;
  password: string;
}
